FROM python:3.11-slim

RUN mkdir /app

COPY . /app
WORKDIR /app

RUN pip3 install .

EXPOSE 8080



CMD ["gunicorn","--config", "gunicorn_config.py", "flask_app:create_app()"]
