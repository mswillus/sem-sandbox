from flask_app import create_app
from flask import Flask
from flask.testing import FlaskClient, FlaskCliRunner

import pytest


@pytest.fixture
def app() -> Flask:
    app = create_app()
    yield app


@pytest.fixture
def client(app: Flask) -> FlaskClient:
    return app.test_client()


@pytest.fixture
def runner(app: Flask) -> FlaskCliRunner:
    return app.test_cli_runner()
