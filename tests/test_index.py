from flask_app import create_app
from flask.testing import FlaskClient

from bs4 import BeautifulSoup


def test_site_is_available(client: FlaskClient) -> None:
    response = client.get("/")
    assert response.status_code == 200


def test_title_is_correct(client: FlaskClient) -> None:
    response = client.get("/")
    soup = BeautifulSoup(response.get_data(), "html.parser")

    assert soup.title.string == "Hello, World!"
