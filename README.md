# SEM Repository (TI3115TU) - 2024
![Test Pipeline](https://github.com/mswillus/sem-example/actions/workflows/python-app.yml/badge.svg)


## Overview

Welcome to the official repository for the git lectures in the 2024 SEM course (TI3115TU) at TU Delft.
This repository is designed to accompany the course lectures and contains example code, exercises, and other resources that will be used throughout the semester.

## Course Information

- **Course Title:** SEM
- **Course Number:** TI3115TU
- **Year:** 2024


### Example Code

You will find example code which we will use and modify to demonstrate the key concepts of git. This code is intended to be a starting point.
You are invited to modify the code and submit merge requests!
As the course progresses, additional examples will be added and existing code may be updated.

## Getting Started

To get started with the git open your git shell and clone the repository!:

1. **Clone the repository** to your local machine:
   ```bash
   git clone https://gitlab.tudelft.nl/mswillus/sem-sandbox
