import time
import sys
from flask import Blueprint, render_template


bp = Blueprint("index", __name__, url_prefix="/")


@bp.route("/")
def index() -> str:
    return render_template("index.html")


def type_text(text: str, delay: float = 0.05) -> None:
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(delay)


def animated_hello(message: str) -> None:
    type_text("Loading", 0.1)

    for _ in range(3):  # Show a loading animation
        for symbol in [" .", " ..", " ..."]:
            sys.stdout.write(symbol)
            sys.stdout.flush()
            time.sleep(0.1)
            sys.stdout.write("\b" * len(symbol))

    print("\n")
    type_text("Here we go.")
    type_text(f"{message} Have a great day! :-)")
